Mardi 31/07/2018 : jour 2

*Warm up ( 12s)*
## L'Environement des projets
Qu'est-ce qui influence nos projets : 
Le contenu du module : 
* les facteurs environnementaux,( il s'agit souvent des entrées dans les processus du PMI- ils sont internes ou externes à l'entreprise)

* les actifs organisationnels: ce que l'entreprise met en place pour servir tous les futurs projets ( les organisations, les templates, les outillages, la connaissance,...)

Le projet se déroule dans un système organisationnel bien particulier qui est celui de l'entreprise (la gouvernance, le management, ...)

### Les Facteurs environnementaux
Tout ce qui dans ou hors de l'entreprise vont avoir une influence positive ou négative sur l'entreprise.


| facteurs internes | facteurs externes |
|-----|----|
| Culture et structure organisationnelles et gouvernance | conditions du marché|
| Dispersion geographique | influences et problèmes sociaux et culturesls|
| infrastructure | restriction légales |
| Logiciels | bases de données commerciales |
| ressources(quantitatives et quantitatives) | considérations financières |




### Les actifs organisationnels
Plus on fait, mieux on fait et plus facilement on fait. On crée des actifs **réutilisables** à chaque fois qu'on fait. 

Le cycle d'eming : la roue d'eming ( l'amélioration continue - )

On a deux catégories d'actifs Orga: 

* Processus, politiques et procédures
    <br/>Ex : liste de prestataires referencés, modèle d'appel d'offre, modèle de NDA, ...

* Bases de connaissnces organisationnelle

Les actifs du premier groupe ( processus...) sont très souvent inchangé et demande à passer par une procédure organisationnelle pour être changé à travers l'aval d'un PMO.
Cela vient habituellement des leçons apprises par l'expérience et au fur et à mesure du temps qu'on éprouve les processus ou qu'il deviennent désuètes.

Ceux du second groupe par contre sont continuellement enrichis au fil des projets.

### Systèmes organisationnels

* La **structures de gouvernance** de l'organisation est l'une des plus grandes contraintes qui s'imposent au CP

Les facteurs du système sont:

* Les élements de management
* Les cadres de gouvernance
* Les types de structure organisationnelle


Principe de base des systèmes : 
* ils sont dynamiques
* ils peuvent être optimisés
* Leurs composants peuvent être optimisés
* **On ne peut optimiser le système et ses composants en même temps**

Le resultat du systèmes vaut mieux la somme des resultats des composants du système.

La somme du tout est plus forte que le tout de la somme.

[La théorie du principe de peter](http://oncle-dom.fr/idees/peter/peter.htm)
Garder la vue holistique sur le système permet de prendre de la hauteur sur le système entier.

Les systèmes(entrée+process+sortie) sont sous la responsabilité d'un management , mais pas sous le rôle du CP.

### Cadres de gouvernance 
Réferentiel COBIT pour ceux qui souhaitent allez plus loin dans la gouvernance.

La gouvernance est le cadre dans lequele l'autorité est exercée dans les organisations.
La gouvernance c'est : 
* les règles, politiques , procédures et normes
* les relations , les systèmes et les processus.

Il s'agit de quelque chose de particulier à chaque structure. 
Ce cadre comprend des élements de management et des pincipaux géneraux.

Les types de structure:
* Organique ou simple
* Fonctionnelle
* Multi-divisions
* Matrice forte
* Matrice faible
* Matrice équilibrée
* Orienté projet
* virtuelle
* hybride
* PMO


### PMO ( project management Office)


il y'a plusieurs types de PMO : 
* support
<br/>consultatif sur le projet, il donne son avis mais n'a pas de contrôle. Il aide le projet dans la mesure du possible.
* Contôlant
<br/>donne du support et réclame de la conformité. Il contrôle et peut rendre compte auprès de la direction ou de la hiérarchie. Il est le garant de la conformité.
* Directif
<br/> il a un contrôle fort sur les projets.

*source p48*

un PMO : est une liaison naturelle entre les portefeuilles, programmes et projets de l'organisation et les systèmes de mesure organisationnels.

Il peut avoir l'autorité d'agir comme une partie prenante et d'être un decideur clé.

*Peu importe son niveau de controle, la fonction PMO reste une fonction support au projet, et il doit servir à la réussite du projet*

## Le rôle du chef de projet

La gestion de projet est un rôle critique et très visible durant toute la durée du proet.

Le chef de projet peut même être impliqué avant le projet, lors des analyses précédents l'initiation avec un analyste business.

Son rôle : 
* Membre et rôles
* responsabilité de l'équipe
* il doit en savoir sur tout mais il n'a pas besoin d'être spécialiste
* il communique une vision
* il doit savoir déleguer et intégrer les résultats.

### Définition et sphère d'influence

En fonction de la sphère le CP remplit divers rôle:
Dans son projet :

* inclure le canaux de feedback
* écouter pour chercher à comprendre
* chercher à comprendre
* créer et maintenir et suivre un plan de communication
* précis, concis et clair
* adapter le discours en fonction de la personne en face

Dans l'organisation :

* interaction proactive avec les autres chefs de projet
    * alignement des objectifs
    * concurrence de financement
    * demandes de mêmes ressources
* Cette interaction aide à créer une influence positive
* la collaboaration avec les manageurs fonctionnels

Dans l'industrie : 

* Faire la veille active sur les sujets
* les outils
* force économiques, amélioration de processus

Dans la discipline professionnelle

* Le transfert de connaissances et l'intégration sotn très important pour le CP ( atélier, hand-on, meet-up)
* parler des expériences

Dans les autres disciplines : 
* montrer l'importance du CP
* Faire comprendre au gens sont rôle


### Les compétences du CP

Le triangle de talent du PMI
* gestion de projet technique
* Leadership
* stratégie et technique

Comment faire pour évoluer sur ces 3 points : **il faut se former**

team : together each achieve more ! 
Les styles de ledership : *slides 45-65*
* politique
* le pouvoir
* le travail fait

Les bons CP sont proactifs et choisissent un style leadership pour leur équipe et ne laisse pas un leadership s'auto-installer.

servant leader : "
travailler , prevenez lorsqu'il y'a des cailloux sur votre passage et je les retirerai ! "

###  L'intégration

c'est la capacité première qu'un chef de projet doit avoir. 
pour cela le CP joue un doube rôle : 
* il collabore
* il oriente l'équipe ( les prios, ...)

-Au niveau des processus : 
C'est de la responsabilité du CP de faire les choix des processus du projet, et de trouver la sequentialié adéquate au projet. 

-Au niveau cognitif :il doit se former pour être sûr de ne pas laisser les passer les aspects cognitifs.(ledership, copétences techniques, expérience, perspicacité)

-Au niveauContextuel : les contextes des projets changent énormement(nearshor, équipes distantes , etc ..., culture, )

-Au niveau complexité : les trois premiers facteurs définissent la compléxité dans son approche système et une certaine complexité est lié à d'autres phénomènes autres que ces 3 premiers.

## Domaines de connaissance
définitions et concepts clé
On ne suivra pas l'ordre naturel des processus dans cette section.

On ne souhaite pas que l'on intègre des processus comme étant séquentiel, mais exécuter plusieurs tous les processus dans différents ordre pour affiner le résultat. *démarche itérative*

### Intégration
il y'a 7 processus qui forme le domaine de l'intégration . 

Les activiés d'intégration se font du début à la fin du projet et implique à chaque fois des ressources. 
il y'a /existe un processus d'intégration pour toutes les phases du projet. 

Les processus d'intégration : 

| initiation | planification | Exécution | surveillance & monitoring | cloture |
| -- | -- | -- | -- | -- |
4.1 developer la charte projet | 4.2 développer le plan de management de projet  | 4.3 diriger le travail du projet | 4.5 Surveiller et contrôler le travail du projet | 4.7 Clôturer la phase ou le projet |
| | | 4.4 manager la connaissance | maitriser les changements | |

### Concepts importants pour l'intégration

**l'intégration** est le domaine de connaissance qui consiste au coeur d'activité du CP. 

Les autres domaines peuvent être délegués mais l'intégration doit rester à la charge du CP. 

L'intégration c'est : 
* Assurer les tenues de dates
* Fournir le plan de management projet
* S'assurer de la création et l'utilisation de la connaissance
* Gérer la performance
* Prendre les décisions
* Mesurer et surveiller
* Gérer les transitions de phases lorsque nécessaire uniquement

### Contenu (Scope)
Comment est-ce qu'on s'assure que le client receptionne bien le contenu envoyer. Que le besoin du client est satisfait.

PMBok présente les processus de contenu coe s'ils étaient isolés mais ils sont en connexion constatne avec tous les autres. 

Le contenu, c'est : 
* le contenu du produit : l'ensemble des fonctionnalités du produit
* le contenu du projet : le travail nécessaire pour livrer le produit , service ou résultat. 
C'est bien souvent des livrables qui ne font pas partie intégrante du produit, ou qui n'ont pas d'impact direct sur le produit. 

Le contenu du projet inclus le contenu du produit

le contenu dépend également du cycle projet (itératif ou préditif).

Tous les élements de contenu à fournir sont définis dans le **scope Baseline**

Les proecessus de contenu sont : 

| initiation | planification | Exécution | Surveillane & Maitrise | Clôture |
| -- | -- | -- | -- | -- |
| | 5.1 planifier le management du contenu | | 5.5 valider le contenu | |
| | 5.2 receuillir les exigences | | 5.6 contrôler le contenu | |
| | 5.3 définir le contenu | | | |
| | 5.4 Créer le wbs | | | |

### Délais (Schedule)


Les processus de délais sont : 


| initiation | planification | Exécution | Surveillane & Maitrise | Clôture |
| -- | -- | -- | -- | -- |
||6.1 planifier le management des délais | | 6.6 Surveiller les délais | |
| | 6.2 Définir les activités | | | |


L'écheancier sert d'outil de communication pour :
* décrire quand le projet sera livrer
* Gérer les attentes des parties prenantes
* Produire des rapports de performance

L'équipe projet doit choisir sa méthode ( agile ou chemin critique).
Les données du projet: 
* activités, dates planifiées , durées , ressources, dépendances, contraintes
* tout cela dans un outil de plannification
* pour produire un modèle d'échéancier du projet.

*Dans des petits projets, les processus de définittion des activités, de séquençage, d'estimation et de développement de l'échéancier sont souvent vus comme un seul processus.*

### Les coûts

P25 pmbok

Les outils de surveillance des coût : 
* la valeur acquise ( **Earned value**)

### Qualité

On a 3 processus qualité, ceci afin d'intégrer les processus qualité de l'entreprise sil y'en a et de garantir un produit de qualité et qui celui attendu par le client.

La qualité c'est aussi veuiller au bon déroulement des processus. Assurer le suivi des processus car ils ont fait leur preuve pour maximiser les chances de réussite d'un projet. 

8.3 et 8.2 interagissent de manière cyclique la plupart du temps. 

#### **Qualité VS la gamme**
il s'agit de 2 concepts différents.
La gamme est l'ensemble des fonctionnalités.

La qualité est un niveau d'adéquation d'un produit avec ses exigeances initiales. 

haut de gamme et basse qualité : c'est un problème
bas de gamme et haute qualité : c'est un choix.

La prévention et l'inspection ne sont pas des concepts équivalents.

Le coût de la qualité : ( CoQ )

### Ressources

matérielles, humaines...
les principaux points sont :
* les identifier
* l'acquisition
* leur gestion ou management
* des ressources nécessaire pour atteindre les objectifs du projet

p25 du pmbok
 Un équipe c'est :
 * des individualités qui ont un rôle
 * le but sera de faire travailler tout le monde ensemble
 * plutôt on fait l'équipe, plus vite on tisse des liens pour mener à bien l'objectif.

Le chef de projet est à la fois un manager et un Leader.

### La communication

La courbe de [kübler Ross](https://nospensees.fr/les-5-phases-du-deuil-de-kubler-ross/)

lorsqu'il y'a des décisions difficiles à faire accepter il faut les annoncer le plutôt possible pour que les gens prennent le temps de traverser les étapes pour atteindre l'acceptation.

La communication c'est l'écahnge d'informations. Elle peut être volontaire ou pas, sous la forme d'idées, d'instructions ou d'émotions.

Elle se fait de plusieurs manière : écrite, parlée, formelle, informelle, gestuelle, par média, avec choix des mots.

Elle peut avoir des dimension multiples.

Plus on communique, plus on limite le risque d'incompréhension. Mais il ne faut pas surcommuniquer ...

Pour communiquer, il faut : 
* Grammaire et orthographe correcte
* expression consise et élimination des mots en trop
* expression clair et dirigée vers les besoins de l'interlocuteur
* contrôle du flux des mots et des idées

Les [5C](https://www.marketing-strategie.fr/2011/07/17/les-5c-de-la-communication-marketing/) : 
* écoute active
* Conscience des différences personnelles et culturelles
* idenditifer 
* ...

### Les Risques

Le but est que pour chaque risque, on va viser à : 
* augmenter la probabilité d'un [risque positif](https://dantotsupm.com/2013/10/28/quest-ce-quun-risque-positif-dans-un-projet/)(opportunités)
* diminuer celle des risque négatif

Pour que le projet soit dans une zone de risque acceptable.

Diagrammes en SWOT

pg 25 pmbok 

#### concepts importants pour les risque
* l'unicité d'un projet fait en sorte que tout projet comporte une part d'inconnu et donc de risque 
* Les risques non-identifiés sont souvent la cause de déviation des plans et in fine de l'échec du projet.
* Les risques existent à deux niveaux : 
    * les risques individuels
    <br/>On prévoti une solution et si le risque ne survient pas on récupère la marge de sécurité comme bénéfice

    * les risques généraux du projet 

qu'il soit individuel ou général , le risque peut être positif ou négatif. 

La mise en place de la solution d'un risque a elle même un risque. 

il faut identifier une certaine tolérance au risque. 

### Approvisionnements

* décrire ce dont on a besoin et à quel moment
* conduire l'approvisionnemet
* surveiller


### Les parties prenantes

* identifier les parties prenantes,
* identifier une stratégie pour informer les bonnes parties prenantes, identifier les parties prenantes difficiles ou contre le projet,
* planifier leur engagement tout au long du projet
* gérer l'engagement des parties prenanntes,
* Maitriser l'engagement des parties prenantes.

Tout projet impacte et est impacté par des individus et des groupes.

Cet impact peut être positif ou négatif.



## 4.1 Élaborer la charte 
En début de projet et (1, 1)

C'est l'élaboration formelle d'un document pour un projet.
La documentation des exigences initiales devant satisfaire les besoins et attentes des parties prenantes.

L'élaboration de la charte lie le projet à la stratégie de l'entreprise. 

Elle a pour fonction : 
* consacrer **officiellement** le lancement du projet
* Préciser les besoins des parties prenantes du projet  

Les projets sont autorisés par une entité ou une personne extérieure au projet. 
* Le commenditaire ( sponsor ) qui appose formellement sa signature au bas de la charte
* le bureau des programmes
* le comex
* un manager

La charte n'est pas un **contrat** , le niveau d'engagement est donc très faible.

| in | tools | output |
| -- | -- | -- |
| **Business case** | Jugement expert | **charte projet** |
|accords | collecte des données | **registre des hypothèses**|
| facteurs environnementaux | compétences interpersonnelles et de gestion d'équipe <br/> -brainstorm <br/> -Focus Group <br/> -interviews | |
actifs organisationnels <br/>-gestion de l'équipe <br/> -gestion des conflits  | Réunions | |

La charte de projet est donc : 
* le besoin business
* les objectifs mesurables et critères de succès
* les exigences à haut niveau
* les risques généraus du projet
* un calendrier résumé de jalons (non engageant bien évidement)
* une liste des principales parties prenantes


### 13.1 identifier les parties prenantes

C'est identifier régulièrement toutes les personnes ou organisations concernées par le projet. 

C'est un document à analyser et documenter; C'est mieux de le faire avant la charte. Pour la bonne raison que le **Qui me demande Quoi ?** nécessite de connaitre le qui.

On les retrouve souvent :
* dans les interviews des premières parties rapidemnt trouvées
* et au fil du projet

On défini une priorité pour les approcher et les meilleurs façons de les influencer.

