# Formation PMP

jour 1 : 30/07/2018
## Le code de conduite

### Les valeurs

Les normes de conduite se définissent selon 4 valeurs :
* Responsabilité
* Respect
* Equité : donne les mêmes chances à chacun
* honnêteté : la transparence etc ...

Le PMI attache des normes à ces valeurs :
* Les normes pragmatiques
    * conduites que le praticien s'effforce de maintenir
    * Difficilemnt mesurables
    * Correspondent aux attentes que nous sommes en droit de nos collègues praticiens
    * Ce n'est pas une option /!\

* Normes obligatoires

### La responsabilité
être responsable c'est prendre des décisions, et les décisions doivent servir l'entreprise.

C'est le chef de projet qui represente le projet, c'est lui qui porte la responsabilité de l'echec ou de la réussite du projet en question.


### Respect
* normes programmatiques

Le respect des cultures également, les origines et la diversité impliquée dans un projet.

Discuter directement avec les personnes avec lesquelles nous sommes en conflit ou en désaccord.

Le point G (GoodWin) de la communication : c'est le point auquel il ne faut jamais arriver.
Comment le résoudre en allant voir la personne et faire de **L'écoute active** , pas dans le répondre mais vraiment dans le but de comprendre la personne en face.

Et toujours rester **PRO**

* Normes obligatoires

négocier en toute bonne foi. Ne pas agir de manière abusive envers les autres.
Nous respectons les droits et la propiété des autres.


### Equité
* Normes programmatiques

Le principal est de faire preuve de transparence. Nous procédons constamement au réexamen de notre **impartialité**. Et ce n'est pas une question d'humeur des personnes autour.

veuiller à ce que l'information soit accessible à tous. Donner toutes le chances à tout le monde.

### Honnetete
Nous cherchons à comprendre la vérité, nous communiquer de manière exacte mais surtout de manière adapté au public en face. 

- encourager à dire la vérité
- prenons des engagements et faisons des promesses, qu'elles soient implicites ou explicites, en toute bonne foi

Normes obligatoires : pas de pratiques malhonnêtes avec l'intention d'en tirer profit aux dépens d'autrui.
Pas de fausses déclarations ou de mensonges.

## PMBok : Program Management Body of Knowledgek

**Adopter pour adapter**

C'est ça le but avec le PMBok contrairement au méthodes ( qui sont plus formels) comme scrum ou Prince2.
Le cadre n'est pas du tout rigide et demande à être contextualisé en fonction des situation. 

Le PMBok est consolidé par le PMI.
Le pmbok n'est pas une méthodologie, mais une fondation pour toute méthodologie. 

### Qu'est-ce qu'un projet  ? 

pas du day to day. 
L'unicité du résultat en un temps , budget imparti.

faire de la créativité, dans un temps limité et avec une initiative limité dans le temps ( début et fin).

> un projet est une entreprise temporaire decidée en vue de produire un résultat uniques produit ou service.

Les caractéristiques sont : 
* l'unicité
* temporalité
* la creation de valeur : le bénefice(tengible ou pas) potentiel pour l'entreprise.

### Importance de la gestion de projet
 On livre le **bon** produit , dans les **délai** et sans **surcoût**.

 Le triptyque projet classique. Il faut être prédictible et satisfaire les parties prenantes pour maximiser es chances de réussir un projet.

 > Qualité = f(scope, coûts, temps)

 La non-qualité est l'ajout de chose inutile à un projet, et cela peut engendrer des couts.
 Golding plating 

 Un temps d'un projet se termine toujours , ne pa confondre la vie d'un projet et la vie d'un produit. Les produits industriels sont souvent industrialisés dans leur cycle de vie produit et pas projet (qui va de la création jusqu'au premier prototype du produit).

*Soit c'est moins bien, soit c'est plus cher, soit c'est plus long.*

*Bon, moins cher, de bonne qualité: choisit en deux*


 ### projet, programme, Portefeuille et Opérations

Le tout est plus que la somme des parties
*Aristote*

Un ensemble de projets groupé pour une stratégie est et coordonné est un programme.

Le protefeuille de projets : c'est l'ensemble de tous les programmes de  l'entreprise.

Le portefeuille est vraiment axé sur les dépenses budgétaires.

*La gestion de portefeuille veille à faire les bons projets et programmes*

*La gestion de programme et de projet veillent à bien faire*

### Management des programmes
la gestion du programme necessite de la coordination entre la gestion des différents projets. Un gestion globale et stratégique des risques.

Un anticipation du changement et une résolutioin des conflits entre les composants.
Et la gestion de l'allocation des budget.

Tout cela pour garantir que le bénefice attendu est au rendez-vous.

### Management de portefeuille

C'est à travers el portefeuille qu'on assure l'obtention des objectifs stratégiques.

Le but d'une gestion de portefeuille est : 
- **Guider** l'organisation dans les investissement
- **Sélectionner** les bons programmes/projets pour un optimum collectif et bénéfique.
- Fournir de la **transparence** dans le processus de décision
- centraliser les **risques**

### Le projet VS l'opérationnel

Le management opérationnel est hors propos dans le modèle projet 

il est orienté sur le day to day. Toutefois il y' a des intersections entre les opérations et les projets, à différents moments du cycle de vie d'un produit. Le transfet de connaissance dans la vie du projet sous forme de livrables.

### OPM ( Management organisationnel de projet)

les 3P doivent alignés ou conduits par la stratégie organisationnel.
* Le portefeuille -> aligne les initiatives aux stratégies
* les programmes -> harmonisent les composants
* les projets -> permettent la réalisation des objectif.

### Composants clé des projets. 
49 processus | 5 groupes | dans 10 domaines de connaissances.

`Le cycle de vie` :
C'est l'ensemble des phases successives d'un projet de son début à la fin

un cycle de vie peut être **adaptatif** ou agile: on ne creuse pas les sujet très longtemps à l'avance. mais on le fait au fur et à mesure de l'avancé et des sujets chauds.

un cycle **prédictif** : on analyse tout à l'avance. ( *waterfall* ou cascade) toutes les phases du projet sont documentés : le contenu, le budget, les délais. 

Le cycle peut aussi être **itératif** : on analyse tout ce qu'on peut et les sujets lointain on le fera plus tard. C'est une planification par vague. C'est une itération (sprint)

Le cycle de vie dit **incrémental** : on ne sait pas ce qu'on veut avec exactitude, mais par contre on a une solution a apporté à un problème.
On incrémente différentes solutions jusqu'à ce rapproché de la solution convenable.

Le cycle **Hybride** : combinaisont du predictif et de l'adaptatif.

### Cycles de vie de projet et de développement

L'équipe doit determiner le meilleur cycle de vie pour chaque projet.

* La flexibilité est nécessaire et permise
    * une identification des processus requis dans chaque phase
    * la mise en oeuvre des processus identifiés dans la phase adéquate
    * L'adjustement des attributs de chaque phase.

Le stand-up : Qu'est-ce que j'ai fait hier ? Qu'est-ce que je fais aujourd'hui ? Qu'est-ce qui me bloque.

### Phase de projet

Les phases sont en séquence et ne doivent pas se chevaucher. 
Les phases ont un ensemble d'attributs au sein du cycle de vies. 
les revues de phases. 

Les **phase Gate** : 
- évaluer la performance de la phase
- comparer le progrès du projet aux documents business.
- décider ( continuer la phase suivante, recommancer, rester sur la même, ..., )

## Processus de Management projet

`processus` : la transformation d'une entrée en livrable (sortie) à travers des outils et technique avec des phases.

`cycle de vie` : c'est l'exécution d'une série de processus;

### Les différentes catégories d'un processus

Les processus sont liés par les **entrées** et les **sorties**. Certains processus ne sont initié **qu'une fois** dans le cycle de vie d'un projet. 
Ex : initier la charte d'un projet.

Certains sont la de manière **réccurente** : la surveillance du risque. 

Certains sont mis en oeuvre en continu tout le long du cycle.

Les différents groupes de processus : 

* La planification
* planning
* execution 
* surveillance et maitrise ( Monitoring )
* cloture

Les groupes sont fait  **Suivant l'axe chronologique** ou **suivant l'axe...**

Les domaine de connaissance**(10 domaines de connaissances) autour desquels sont organisés les processus.

Les 10 domaines de connaissances sont : 
* Intégration ( chap 4)
* contenu
* Délais
* Coûts
* Qualité
* risque
* approvisionement
* 

*Source PMBok chap 3 fig 3-2.* et page 25 du PMBok

La méthodologie doit être le choix du chef de projet, autant sur les processus, que sur les outils, et les entrés pour avoir les sorties escomptées.

La personnalisation du projet est unique. Parce qu'il faut tenir compte des contraintes spécifiqques de contenu. 

### documents business : Business case
Business case : Etude de faisabilité économique documenté établisant la validité des bénéfices du projet. 
 
 Le BS donne la liste des objectifs à l'initiation d'un projet : 
 * il aide à mesurer le succès du projet
 * mesurer le progrès réalisé
 
 Son contenu est : 
 * le besoin métier  : ce qu'on veut , les opportunités , ETC
 * l'analyse de la situation : les critères pour mettre en place des actions , les scénariis, 
 * recommendations : les solutions possibles, le choix de la solution, les options possibles, le resultat de l'analyse de risque et des contraintes. Un exemple d'implémentation très haut niveau et des résultats non-engageants.
 *Évaluation 

 ### Le plan de management de bénéfices

Le but est de la création agile de la valeur.

 Quand et comment on créer du bénefice et comment on le mesure ? 

Les document clés de l'entreprise ? 

1. project charter : la charte projet

la charte de projet est le document fondateur du projet.
Elle donne une existence formelle au projet et l'autorité au chef de projet.
Elle énon les objectifs du projet et le périmètre du produit, service ou résultat attendu à l'issue du projet. 

2. project management plan

Il sert de guide au projet du début jusqu'à la fin .

### Mesure de succès du projet

* Comment évaluer un projet ? 
    * Traditionnellement : c'est le cout , le temps et la qualité 
    * plus récemment : la satisfaction client, des critères mesurables. ( NPV, ROI, IRR, PBP, BCR )

Les succès de l'équipe, ne sont pas necessairement les succès du client. 
L'**alignement** est important pour se rassurer que le succès est au rendez-vous pour tous. 

